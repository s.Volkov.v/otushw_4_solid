﻿using System;
using HomeWork_SOLID.Interfaces;

namespace HomeWork_SOLID
{
    public class NumberChecker: INumberChecker
    {
        public NumberCheckerResult CheckNumber(int secretNumber,int number)
        {
            if (secretNumber < number) { return NumberCheckerResult.less; }
            if (secretNumber > number) { return NumberCheckerResult.greater; }
            return NumberCheckerResult.equal;
        }
    }
}
