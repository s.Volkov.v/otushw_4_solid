﻿using System;
namespace HomeWork_SOLID.Interfaces
{
    public interface IGameConsole
    {
        public void StartNewGame();
    }

}

