﻿using System;
namespace HomeWork_SOLID.Interfaces
{
    public interface INumberGenerator
    {
        public int GetSecretNumber();
    }
}

