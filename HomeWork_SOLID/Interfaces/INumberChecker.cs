﻿using System;
namespace HomeWork_SOLID.Interfaces
{
    public interface INumberChecker
    {
        NumberCheckerResult CheckNumber(int secretNumber, int number);
    }
}

public enum NumberCheckerResult
{
    equal = 0,
    less = 1,
    greater = 2
}

