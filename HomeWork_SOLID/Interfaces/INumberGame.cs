﻿using System;
namespace HomeWork_SOLID.Interfaces
{
    public interface INumberGame: IGame
    {
        public int GetSecretNumber();

        public int GetTryCount();

        public int GetTryLimit();

        public string CheckNumber(int Number);

    }
}

