﻿using System;
namespace HomeWork_SOLID.Interfaces
{
    public interface IGame
    {
        public string StartGame();

        public string StopGame();

        public bool CkeckGameIsRunning();

        public bool CheckGameFinished();
    }
}

