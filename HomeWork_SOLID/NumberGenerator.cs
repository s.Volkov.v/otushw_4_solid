﻿using System;
using HomeWork_SOLID.Interfaces;

namespace HomeWork_SOLID
{
    public class NumberGenerator: INumberGenerator
    {
        private readonly int _min;

        private readonly int _max;

        public NumberGenerator(int min, int max)
        {
            _min = min;
            _max = max;
        }

        public virtual int GetSecretNumber()
        {
            if (_min < 0) { throw new Exception("Указано число меньше 0"); }
            if (_max > 100000) { throw new Exception("Указано число больше 100000"); }
            var rnd = new Random();
            return rnd.Next(_min, _max);
        }
    }
}

