﻿using HomeWork_SOLID;
using HomeWork_SOLID.Interfaces;

while (true)
{
    Console.WriteLine("Выберите игру:");
    Console.WriteLine("1 - Угадай число.");
    Console.WriteLine("0 - не хочу уграть.");
    int gameNumber;
    if (!int.TryParse(Console.ReadLine(), out gameNumber))
    {
        Console.WriteLine("Необходимо указать номер игры.\n\n");
    }
    bool gameStart = true;
    IGameConsole Game;
    if (gameNumber == 1) {
        Game = new NumberGameConsole();
        Game.StartNewGame();
    }
    if (gameNumber == 0) {
        break; 
    }
    
}
Console.WriteLine("Выход из программы");

