﻿using System;
using HomeWork_SOLID.Interfaces;

namespace HomeWork_SOLID
{
    public class NumberGameConsole: IGameConsole
    {
        private INumberGame _numberGame;

        public void StartNewGame()
        {
            int minNumber;
            int maxNumber;
            int tryCount;
            while (true)
            {
                Console.WriteLine("Укажите минимальное число:");

                if (int.TryParse(Console.ReadLine(), out minNumber))
                {
                    break;
                }
                Console.WriteLine("Необходимо минимальное число, но не меньше 0.\n");
            }
            while (true)
            {
                Console.WriteLine("Укажите максимальное число:");

                if (int.TryParse(Console.ReadLine(), out maxNumber))
                {
                    break;
                }
                Console.WriteLine("Необходимо максимальное число, но не больше 100000.\n");
            }
            while (true)
            {
                Console.WriteLine("Укажите максимальное кол-во попыток:");

                if (int.TryParse(Console.ReadLine(), out tryCount))
                {
                    break;
                }
                Console.WriteLine("Необходимо максимальное число, но не меньше 0.\n");
            }

            NumberGenerator numberGenerator = new NumberGenerator(minNumber, maxNumber);
            INumberChecker numberChecker = new NumberChecker();
            _numberGame = new NumberGame(numberGenerator,numberChecker,tryCount);
            Console.WriteLine(_numberGame.StartGame());
            Console.WriteLine($"Было сгенерировано случайно число: {minNumber} > ? > {maxNumber}");
            NumberGameMain();
        }

        private void NumberGameMain()
        {
            do{
                if (_numberGame.GetTryCount() != 0) { Console.WriteLine($"Было произведено попыток: {_numberGame.GetTryCount()}"); }
                Console.WriteLine($"Введите предпологаемое число: ");
                int playerNumber;
                if (!int.TryParse(Console.ReadLine(), out playerNumber))
                {
                   Console.WriteLine("Необходимо ввести число.");
                    continue;
                }
                NumberCheck(playerNumber);
            } while (!_numberGame.CheckGameFinished()) ;
            NumberGameFinish();
        }

        private void NumberGameFinish()
        {
            do
            {
                if (_numberGame.GetTryCount() == _numberGame.GetTryLimit())
                {
                    Console.WriteLine($"Закончились попытки, загадное число было: {_numberGame.GetSecretNumber()} ");
                }
                else { Console.WriteLine($"Число: {_numberGame.GetSecretNumber()}, было угадано с {_numberGame.GetTryCount()} попытки.\n\n"); }
                Console.WriteLine("Нажмите любую кнопку чтобы выйти или 1, чтобы повторить");
                int playerSelect;
                if (int.TryParse(Console.ReadLine(), out playerSelect))
                {
                    if (playerSelect == 1) { StartNewGame(); }
                }
                _numberGame.StopGame();
            } while (_numberGame.CkeckGameIsRunning());
            
        }

        private void NumberCheck(int playerNumber)
        {
            Console.WriteLine(_numberGame.CheckNumber(playerNumber));
        }

    }
}

